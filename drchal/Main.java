/**
 * Created by drchajan on 05/03/14.
 */
public class Main {
    public int f(int x) {
        //2,3,4,5
        if (x <= 0) {
            return 0;
        }
        int y = x + f(x - 1);
        //6,7,8
        return y;
    }

    public int f2(int x) {
        int y = 0;
        int i = 1;
        while (i <= x) {
            y += i;
            i++;
        }
        return y;
    }

    public static void main(String[] args) {
        Main main = new Main();
        //1
        int y = main.f(3);
        //9
        System.out.println(y);
    }
}
